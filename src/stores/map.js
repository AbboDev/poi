import { defineStore } from "pinia";

export const useMapStore = defineStore("map", {
  state: () => ({
    points: [],
    center: [45.69173591, 9.23902452],
  }),
  getters: {
    getPoint: (state) => {
      return (id) => {
        return state.points.find((item) => {
          return item.id === id;
        });
      };
    },
  },
  actions: {
    updateCenter(center) {
      if (
        Array.isArray(center) &&
        center.length === 2 &&
        !isNaN(center[0]) &&
        !isNaN(center[1])
      ) {
        this.center = center;
      }
    },

    updateCenterById(id) {
      const point = this.getPoint(id);

      if (point) {
        this.center = point.center;
      }
    },

    push(point) {
      if (typeof point.center === "undefined") {
        point.center = this.center;
      }

      this.points.push(point);
    },

    pushMultiple(points) {
      for (const point of points) {
        if (typeof point.center === "undefined") {
          point.center = this.center;
        }
      }

      this.points = this.points.concat(points);
    },

    update({ id, data }) {
      const point = this.getPoint(id);

      if (point) {
        const index = this.points.indexOf(point);

        this.points[index] = {
          ...this.points[index],
          ...data,
        };
      }
    },

    remove(id) {
      const point = this.getPoint(id);

      if (point) {
        const index = this.points.indexOf(point);

        this.points.splice(index, 1);
      }
    },
  },
});
